package com.flowz.projecta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.flowz.projecta.databinding.ActivityMainBinding;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding activityMainBinding;


    //    TextView date, time;
//    Button selectDate, selectTime;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Timer timer;
    int number = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        final Changedateandtime changedateandtime = new Changedateandtime();


        DataModel dataModel = new DataModel("This is databinding", R.drawable.ic_assignment_ind_black_24dp, "Two way databinding");
        activityMainBinding.setDataModel(dataModel);

//        activityMainBinding.imageView.setImageResource(dataModel.getImage());
//        activityMainBinding.text1.setText(dataModel.getName());
//        activityMainBinding.text2.setText(dataModel.getSummary());

        timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (number==0){

                            DataModel dataModel = new DataModel("Hello there", R.drawable.ic_assignment_ind_black_24dp, "This is databinding");
                            activityMainBinding.setDataModel(dataModel);
//
//                            activityMainBinding.imageView.setImageResource(dataModel.getImage());
//                            activityMainBinding.text1.setText(dataModel.getName());
//                            activityMainBinding.text2.setText(dataModel.getSummary());

                            number = 1;
                        } else {
                            number= 0;

                            DataModel dataModel = new DataModel("Welcome to databinding", R.drawable.ic_assignment_ind_black_24dp, "Learn to use databinding");
                            activityMainBinding.setDataModel(dataModel);

//                            activityMainBinding.imageView.setImageResource(dataModel.getImage());
//                            activityMainBinding.text1.setText(dataModel.getName());
//                            activityMainBinding.text2.setText(dataModel.getSummary());
                        }
                    }
                });
            }
        }, 5000, 5000);


//        date = findViewById(R.id.date1);
//        time = findViewById(R.id.time1);
//        selectDate = findViewById(R.id.datePicker);
//        selectTime = findViewById(R.id.timePicker);


       activityMainBinding.datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               selectDate();
            }
        });
        activityMainBinding.timePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               selectTime();
            }
        });

    }

    @BindingAdapter({"android:src"})
    public static void setImageViewResource(ImageView imageView, int resource){
        imageView.setImageResource(resource);
    }



    private void selectTime() {

        //GetCurrentTime

            final Calendar cal = Calendar.getInstance();
            mHour = cal.get(Calendar.HOUR_OF_DAY);
            mMinute = cal.get(Calendar.MINUTE);

            //Launch TimePickerDialog

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i1) {
                   activityMainBinding.time1.setText(i + ":" + i1);
                }
            }, mHour,mMinute, false);
            timePickerDialog.show();

    }

    private void selectDate() {

        //GetCurrentDate
            final Calendar cal = Calendar.getInstance();
            mYear = cal.get(Calendar.YEAR);
            mMonth = cal.get(Calendar.MONTH);
            mDay = cal.get(Calendar.DAY_OF_MONTH);

            //Launch DatePickerDialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                       activityMainBinding.date1.setText(  i2 + "-" + (i1+1) + "-" + i);
                    }
                }, mYear, mMonth, mDay);

                datePickerDialog.show();
        }

    }

//    @Override
//    public void onClick(View view) {
//
//        if (view==selectDate){
//            //GetCurrentDate
//            final Calendar cal = Calendar.getInstance();
//            mYear = cal.get(Calendar.YEAR);
//            mMonth = cal.get(Calendar.MONTH);
//            mDay = cal.get(Calendar.DAY_OF_MONTH);
//
//            //Launch DatePickerDialog
//                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
//                        date.setText(  i2 + "-" + (i1+1) + "-" + i);
//                    }
//                }, mYear, mMonth, mDay);
//
//                datePickerDialog.show();
//        }
//        if (view==selectTime){
//            //GetCurrentTime
//
//            final Calendar cal = Calendar.getInstance();
//            mHour = cal.get(Calendar.HOUR_OF_DAY);
//            mMinute = cal.get(Calendar.MINUTE);
//
//            //Launch TimePickerDialog
//
//            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
//                @Override
//                public void onTimeSet(TimePicker timePicker, int i, int i1) {
//                    time.setText(i + ":" + i1);
//                }
//            }, mHour,mMinute, false);
//            timePickerDialog.show();
//        }
////
//    }
//}
