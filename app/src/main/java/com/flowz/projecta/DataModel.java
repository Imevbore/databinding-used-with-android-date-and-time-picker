package com.flowz.projecta;

public class DataModel {

    String name;
    int image;
    String summary;

    public DataModel(String name, int image, String summary) {
        this.name = name;
        this.image = image;
        this.summary = summary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
